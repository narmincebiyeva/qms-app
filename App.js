import React, {useState,} from 'react';
import { StyleSheet,View } from 'react-native';
import {AppLoading} from "expo";


import {loadFonts} from "./styles/fonts";
import {RootNav} from "./navigation"
import { DatePicker } from "./components/DatePicker"
import {HomeScreen} from "./screens/HomeScreen"

export default function App() {

  const [loaded,setLoaded] = useState(false);
  if(!loaded){
    return <AppLoading
            startAsync={loadFonts}
            onFinish={()=> setLoaded(true)}
            onError={()=>console.log("rejected")}
    />
  }

  return (
      <HomeScreen/>
    
  );
}

