import React from 'react';
import { ScrollView, StyleSheet,View, Image, FlatList } from 'react-native';


import {BottomNav} from "../../components/BottomNav"
import { Txt } from '../../components/Txt';
import { Ticket } from '../../components/Ticket';

const DATA = {
  name: "Aydan",
  surname: "Haqverdili",
  services: "Valyuta tenzimlemesi",
  branch: "Nerimanov filialı",
  time : "10 september 12pm-1am",
  operator: "Meryem Memmedzade",
  place: "14"
}


export const TicketScreen = () => {


  return (
    <View style={{flex: 1}}>
      <View style={styles.header}>
            <Image source={{uri: "https://lh3.googleusercontent.com/-giN8Q_viK6eN4xqd9oXd1hcSq7RfbLA6TMEr_G8Kyx-OkBJGfqAQisoay7bGMZcrpc"}} style={styles.img}/>
            <Txt style={styles.name}>filial </Txt>
      </View>
     
      <View style={styles.container}>
        <Ticket data={DATA}/>   
      
        <BottomNav/>
      </View>     
  </View>
      
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    paddingHorizontal: 16,
    overflow: "hidden",
  },
  header: {
    backgroundColor: "#2A58A2",
    alignItems: "center",
    flexDirection: "row",
    height: 70,
    width: "100%",
    paddingHorizontal: 16
  },
  name: {
    fontSize: 24,
    color: "#fff",
 },
  img: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 16
  },
 
});
