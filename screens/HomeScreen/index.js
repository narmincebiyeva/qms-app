import React from 'react';
import { ScrollView, StyleSheet,View, Image, FlatList ,TouchableOpacity} from 'react-native';
import {LinearGradient} from "expo-linear-gradient";

import {BottomNav} from "../../components/BottomNav"
import { Txt } from '../../components/Txt';
import sun from "../../assets/images/sun.png"
import { Company } from '../../components/Company';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'Azərbaycan beynəlxalq bankı',
    services: [
      {
        title: "valyuta",
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63'
      },
      {
        title: "pul köçürme",
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63'
      },
      {
        title: "kassa emeliyyatları",
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63'
      },
    ]
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Azericard',
    services: [
      {
        title: "valyuta",
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63'
      },
      {
        title: "pul köçürme",
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63'
      },
      {
        title: "kassa emeliyyatları",
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63'
      },
    ]
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Baku Medical Plaza',
     services: [
      {
        title: "valyuta",
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63'
      },
      {
        title: "pul köçürme",
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63'
      },
      {
        title: "kassa emeliyyatları",
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63'
      },
    ]
  },
 
  

];


export const HomeScreen = ({navigation}) => {
  return (
      <View style={styles.container}>
       
        <LinearGradient colors={[ '#FFFFFF','#2A58A2']} style={styles.linearGradient}>
          <Txt style={styles.greeting}>Good morning, {"name"}</Txt>
          <Image source={sun} style={styles.img}/>
        </LinearGradient>

        <FlatList
        style={styles.companies}
        data={DATA}
        renderItem={({item}) => <TouchableOpacity onPress={() => navigation.navigate("company",{services: item.services, title: item.title})}>
                                    <Company data={item}/>
                                </TouchableOpacity>}
        keyExtractor={item => item.id}
      />

        <BottomNav/>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E5E5E5',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 16
  },
  linearGradient: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    height: 70,
    alignItems: "center",
    justifyContent: "space-evenly",
    flexDirection: "row",
    paddingHorizontal: 40,
  },
  greeting: {
    fontSize: 24,
    color: "#fff"
  },
  img: {
    width: 40,
    height: 40,
  },
  companies: {
    marginTop: 100,
    width: "100%",
    
  }
});
