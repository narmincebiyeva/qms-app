import React from 'react';
import { ScrollView, StyleSheet,View, Image, FlatList } from 'react-native';


import {BottomNav} from "../../components/BottomNav"
import { Txt } from '../../components/Txt';
import { Service } from '../../components/Service';




export const CompanyScreen = ({route}) => {

  const { services,title } = route.params;

  return (
    <View style={{flex: 1}}>
      <View style={styles.header}>
            <Image source={{uri: "https://lh3.googleusercontent.com/-giN8Q_viK6eN4xqd9oXd1hcSq7RfbLA6TMEr_G8Kyx-OkBJGfqAQisoay7bGMZcrpc"}} style={styles.img}/>
            <Txt style={styles.name}>{title} </Txt>
      </View>
     
      <View style={styles.container}>   
       <FlatList
        style={styles.services}
        data={services}
        renderItem={({item}) => <Service data={item}/>}
        keyExtractor={item => item.id}
      />

        <BottomNav/>
      </View>
    </View>
      
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E5E5E5',
    paddingHorizontal: 16
  },
  header: {
    backgroundColor: "#2A58A2",
    alignItems: "center",
    flexDirection: "row",
    height: 70,
    width: "100%",
    paddingHorizontal: 16
  },
  name: {
    fontSize: 24,
    color: "#fff",
 },
  img: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 16
  },
  services: {
    width: "100%",
    
  }
});
