import React, {useState} from 'react';
import { StyleSheet,Image,Text, ScrollView} from 'react-native';
import {LinearGradient} from "expo-linear-gradient";

import {Btn,} from "../../components/Btn"
import {Txt} from "../../components/Txt"
import {Field} from "../../components/Field"
import Logo from "../../assets/images/logo.png"
import email from "../../assets/images/email.png"
import password from "../../assets/images/password.png"


export const RegisterScreen = () => {

  
  const [fields, setFields] = useState({
    name: "",
    surname: "",
    fin: "",
    phone: "",
    email: "",
    password: "",
    roleName: "customer"
  });

  const handleFieldChange = (name, value) => {
    setFields((fields) => ({
      ...fields,
      [name]: value,
    }));
    
  }; 

  const submitFields = () => {
    // validation and submit to store
  }

  return (
    <LinearGradient colors={[ '#FFFFFF','#2A58A2']} style={styles.linearGradient}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ flexGrow: 1, justifyContent: 'center',alignItems: "center" }}
        >
            <Txt style={styles.header}>register</Txt>
            <Image source={Logo} style={styles.logo}/>
            <Field
             name="name"
             value={fields.name}
             handleFieldChange={handleFieldChange}
            />
            <Field
             name="surname"
             value={fields.surname}
             handleFieldChange={handleFieldChange}
            />
            <Field
             name="fin"
             value={fields.fin}
             handleFieldChange={handleFieldChange}
            />
            <Field
             name="phone"
             value={fields.phone}
             handleFieldChange={handleFieldChange}
            />
            <Field
             name="email"
             value={fields.email}
             handleFieldChange={handleFieldChange}
            />
            <Field 
             secure={true}
             name="password"
             value={fields.password}
             handleFieldChange={handleFieldChange}
            />
    
            <Btn
             title="register"
             style={styles.btn}
             onPress={() => submitFields()}/> 
        </ScrollView>
        
    </LinearGradient>
   
  );
}

const styles = StyleSheet.create({
  linearGradient: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    height: '100%',
    width:'100%',
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 40,
  },
  header: {
    color: "#fff",
    fontSize: 64,
    textTransform: "capitalize",
    marginTop: 100
  },
  logo: {
    width: 140,
    height: 130,
    marginVertical: 30
  },
  btn: {
    marginTop: 50,
    marginBottom: 30
  }
});
