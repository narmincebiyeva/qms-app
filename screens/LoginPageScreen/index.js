import React, {useState} from 'react';
import { StyleSheet,Image,Text} from 'react-native';
import {LinearGradient} from "expo-linear-gradient";

import {Btn,} from "../../components/Btn"
import {Txt} from "../../components/Txt"
import {Field} from "../../components/Field"
import Logo from "../../assets/images/logo.png"
import email from "../../assets/images/email.png"
import password from "../../assets/images/password.png"


export const LoginPageScreen = () => {

  
  const [fields, setFields] = useState({
    email: "",
    password: "",
  });

  const handleFieldChange = (name, value) => {
    setFields((fields) => ({
      ...fields,
      [name]: value,
    }));
    
  }; 

  const submitFields = () => {
    // validation and submit to store
  }

  return (
    <LinearGradient colors={[ '#FFFFFF','#2A58A2']} style={styles.linearGradient}>
         <Txt style={styles.header}>log in</Txt>
         <Image source={Logo} style={styles.logo}/>
         <Field
          img={email}
          name="email"
          value={fields.email}
          handleFieldChange={handleFieldChange}
         />
         <Field 
           secure={true}
           img={password}
           name="password"
           value={fields.password}
           handleFieldChange={handleFieldChange}
         />
    
         <Btn
          title="log in"
          style={styles.btn}
          onPress={() => submitFields()}/>
    </LinearGradient>
   
  );
}

const styles = StyleSheet.create({
  linearGradient: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    height: '100%',
    width:'100%',
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 40,
  },
  header: {
    color: "#fff",
    fontSize: 64,
    textTransform: "capitalize",
  },
  logo: {
    width: 140,
    height: 130,
    marginVertical: 30
  },
  btn: {
    marginTop: 50
  }
});
