import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { LoginPageScreen} from "../screens/LoginPageScreen";
import {CompanyScreen} from "../screens/CompanyScreen";
import {RegisterScreen,} from "../screens/RegisterScreen";
import {HomeScreen} from "../screens/HomeScreen";

const { Navigator, Screen } = createStackNavigator();

export const HomeStack = () => (
  <Navigator headerMode ="none">
    <Screen name="home" component={HomeScreen} />
    <Screen name="login" component={LoginPageScreen} />
    <Screen name="register" component={RegisterScreen} />
    <Screen name="company" component={CompanyScreen} />    
  </Navigator>
);
