import React from "react";
import {Image, TouchableOpacity, View, StyleSheet} from 'react-native';

import {Txt} from "./Txt"
import arrow from "../assets/images/arrow.png"

export const Company = ({data}) => {
return (
 
    <View style={styles.container}>                 
        <Image style={styles.img} source={{uri: "https://lh3.googleusercontent.com/-giN8Q_viK6eN4xqd9oXd1hcSq7RfbLA6TMEr_G8Kyx-OkBJGfqAQisoay7bGMZcrpc"}}/>
        <Txt style={styles.name}>{data.title}</Txt>
        <View style={styles.circle}>
        <View style={styles.rectangle}/>
          <Image source={arrow} style={styles.arrow}/>
        </View>      
    </View>
)
}

const styles = StyleSheet.create({
    container: {
      height: 100,
      flexDirection: "row",
      backgroundColor: '#fff',
      alignItems: "center",
      justifyContent: "space-between",
      shadowColor: "#000",
      shadowOffset: {
          width: 0,
          height: 3,
      },
      shadowOpacity: 0.27,
      shadowRadius: 4.65,
      
      elevation: 6,
      marginVertical: 15
      
    },
    img: {
      width: 70,
      height: 70
    },
    name: {
        fontSize: 20,
    },
    rectangle: {
       width: 50,
       height: "100%",
       backgroundColor: "#2D54A8",
       alignSelf: "flex-end"
    },
    circle: {
      position: "relative",
        width: 95,
        height: "100%",
        borderRadius: 50,
        backgroundColor: "#2D54A8",
    },
    arrow: {
      position: "absolute",
      width: 40,
      height: 60,
      top: 20,
      left: 30
    }
  
  });
  