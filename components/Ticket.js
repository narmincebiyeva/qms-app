import React from 'react';
import {  StyleSheet,View,  } from 'react-native';

import {Btn} from "../components/Btn"
import {Txt} from "../components/Txt"





export const Ticket = ({data}) => {


  return (
        <View style={styles.ticket}>
            <View style={styles.topCircle}/>
            <View style={styles.bottomCircle}/>
           
            <Txt style={styles.ticketNo}>Ticket 24</Txt>
           
           <View style={{marginVertical: 10}}>
             <View style={styles.userDataWrapper}>
              <Txt style={styles.userDataName}>name :</Txt>
              <Txt style={styles.userData}>{data.name}</Txt>
            </View>
            <View style={styles.userDataWrapper}>
              <Txt style={styles.userDataName}>surname :</Txt>
              <Txt style={styles.userData}>{data.surname}</Txt>
            </View>
            <View style={styles.userDataWrapper}>
              <Txt style={styles.userDataName}>services :</Txt>
              <Txt style={styles.userData}>{data.services}</Txt>
            </View>
            <View style={styles.userDataWrapper}>
              <Txt style={styles.userDataName}>branch :</Txt>
              <Txt style={styles.userData}>{data.branch}</Txt>
            </View>
            <View style={styles.userDataWrapper}>
              <Txt style={styles.userDataName}>time :</Txt>
              <Txt style={styles.userData}>{data.time}</Txt>
            </View>
            <View style={styles.userDataWrapper}>
              <Txt style={styles.userDataName}>operator :</Txt>
              <Txt style={styles.userData}>{data.operator}</Txt>
            </View>
            <View style={styles.userDataWrapper}>
              <Txt style={styles.userDataName}>place :</Txt>
              <Txt style={styles.userData}>{data.place}</Txt>
            </View>
           </View>
            
          
            <Btn title="Agree and Take"/>

        </View>    
    );
}

const styles = StyleSheet.create({
  
  ticket: {
    borderWidth: 15,
    borderColor: "#2A58A2",
    marginTop: 25,
    height: "80%",
    marginHorizontal: 20,
    position: "relative",
    paddingVertical: 60,
    paddingHorizontal: 30
  },
  topCircle: {  
      position: "absolute",    
     backgroundColor: "#fff",
     alignSelf: "center",
     top: -85,
     width: 140,
     height: 140,
     borderWidth: 15,
     borderBottomColor: "#2A58A2",
     borderRightColor: "#2A58A2",
     borderLeftColor: "transparent",
     borderTopColor: "transparent",
     transform: [
        { rotate: "45deg" }
      ],
     borderRadius: 70

  },
  bottomCircle: {    
      position: "absolute",  
    backgroundColor: "#fff",
    alignSelf: "center",
    bottom: -85,
    width: 140,
    height: 140,
    borderWidth: 15,
    borderTopColor: "#2A58A2",
    borderLeftColor: "#2A58A2",
    borderRightColor: "transparent",
    borderBottomColor: "transparent",
    transform: [
       { rotate: "45deg" }
     ],
    borderRadius: 70

 },
 ticketNo: {
   fontSize: 24,
   color: "#2A58A2",
   alignItems: "center",
   justifyContent: "center",
   textAlign: "center"
 },
 userData: {
  fontSize: 12,
  borderBottomColor: "#2A58A2",
  borderBottomWidth: 2,
  width: "70%",
  paddingLeft: 10
 },
 userDataName: {
    fontSize: 11,
    color: "#2A58A2",
    width: "30%"

 },
 userDataWrapper: {
  flexDirection: "row",
  marginVertical: 7,
  alignItems:"center",
  width: "100%"
 }
});
