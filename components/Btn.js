import React from 'react';
import { StyleSheet, View,TouchableOpacity,TouchableNativeFeedback,Platform} from 'react-native';

import {Txt} from "./Txt";

export const Btn = ({title,onPress,style,...rest}) => {
    const Touchable = Platform.OS === "android" ? TouchableNativeFeedback : TouchableOpacity;
    return(
        <View style={[style,styles.container]}>
            <Touchable onPress={onPress} {...rest} >
                <View style={styles.btn}>
                    <Txt weight="bold" style={styles.title}>
                        {title}
                    </Txt>
                </View>
            </Touchable>
        </View>
    );
};

const styles = StyleSheet.create({
    container:{
        width: "100%",
        justifyContent:"center",
        overflow:"hidden",
        borderRadius:40,
    },
    btn:{
        width:"100%",
        padding:15,
        alignItems: "center",
        backgroundColor: "#2D54A8"
    },        
    title:{
        textTransform:"capitalize",
        color:"white",
        textAlign: "center",
        fontSize:20,
    },

});