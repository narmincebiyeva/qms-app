import React, {useState} from "react";
import { View,TextInput,StyleSheet ,TouchableOpacity,Image } from "react-native";

import openEye from "../assets/images/openEye.png" 
import closeEye from "../assets/images/closeEye.png" 


export const Field = ({value,handleFieldChange,secure,img,name}) => {
   
    const [passIsHide,setPassIsHide] = useState(secure)
    
    
return (
    <View style={styles.container}>
        <View style={styles.imgContainer}>     
            <Image
              style={styles.img}
              source={img}
            />
      </View>

    <TextInput
      style={styles.input}
      value={value}
      onChangeText={(v) => handleFieldChange(name,v)}
      secureTextEntry={passIsHide}
      placeholder={name}
      placeholderTextColor="#fff"
    />
    {
        secure && 
        <View style={styles.passwordHideToggleElement}>
        <TouchableOpacity onPress={() => setPassIsHide((v) => !v)}>
          {passIsHide ? (
            <Image
              style={styles.passwordHideToggleElementImg}
              source={closeEye}
            />
          ) : (
            <Image
              style={styles.passwordHideToggleElementImg}
              source={openEye}
            />
          )}
        </TouchableOpacity>
      </View>
    }

    </View>
)
};

const styles = StyleSheet.create({
    container:{
        width: "100%",
        justifyContent:"center",
        overflow:"hidden",
        marginVertical: 10,
        position: "relative"
    },
    input: {
        paddingVertical:5,
        borderColor: '#fff',
        borderWidth: 1 ,
        borderRadius: 40,
        color: "#fff",
        paddingHorizontal: 60
    },
    passwordHideToggleElement: {
        position: "absolute",
        width: 19,
        height: 19,
        right: 15
        
      },
      passwordHideToggleElementImg: {
        width: 19,
        height: 19,
      },
      imgContainer: {
        position: "absolute",
        width: 19,
        height: 23,
        left: 15
        
      },
      img: {
        width: 19,
        height: 23,
      },
    

});