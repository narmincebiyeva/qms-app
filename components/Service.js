import React from "react";
import {Image, TouchableOpacity, View, StyleSheet} from 'react-native';

import {Txt} from "./Txt"
import arrow from "../assets/images/arrow2.png"


export const Service = ({data}) => {
return (
 
    <View style={styles.container}>                 
        <Txt style={styles.name}>{data.title}</Txt>
        <Image source={arrow}  style={styles.arrow}/>
    </View>
)
}

const styles = StyleSheet.create({
    container: {
      height: 60,
      flexDirection: "row",
      backgroundColor: '#fff',
      alignItems: "center",
      justifyContent: "space-between",
      shadowColor: "#000",
      shadowOffset: {
          width: 0,
          height: 3,
      },
      shadowOpacity: 0.27,
      shadowRadius: 4.65,
      
      elevation: 6,
      marginVertical: 10
      
    },
    name: {
        fontSize: 20,
        marginHorizontal: 40
    },
    arrow: {
      width: 40,
      height: 50,
      marginHorizontal: 5
    
    }
  
  });
  